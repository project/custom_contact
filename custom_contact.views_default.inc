<?php
function custom_contact_views_default_views() {
$views = array();

$view = new view;
$view->name = 'custom_contact';
$view->description = 'Custom Contact list pages, including a user-facing contact list and an administration view for adding/removing users from the list.';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'users';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'flag_content_rel' => array(
    'label' => 'Contact page',
    'required' => 0,
    'flag' => 'custom_contact',
    'user_scope' => 'any',
    'id' => 'flag_content_rel',
    'table' => 'users',
    'field' => 'flag_content_rel',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'flag_content_rel_1' => array(
    'label' => 'Anonymous contact',
    'required' => 0,
    'flag' => 'anonymous_contact',
    'user_scope' => 'any',
    'id' => 'flag_content_rel_1',
    'table' => 'users',
    'field' => 'flag_content_rel',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'picture' => array(
    'label' => 'Picture',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'exclude' => 0,
    'id' => 'picture',
    'table' => 'users',
    'field' => 'picture',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'name' => array(
    'label' => 'Name',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'link_to_user' => 1,
    'overwrite_anonymous' => 0,
    'anonymous_text' => '',
    'exclude' => 0,
    'id' => 'name',
    'table' => 'users',
    'field' => 'name',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'rid' => array(
    'label' => 'Roles',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'type' => 'separator',
    'separator' => ', ',
    'exclude' => 0,
    'id' => 'rid',
    'table' => 'users_roles',
    'field' => 'rid',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'custom_contact' => array(
    'label' => 'Contact',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'text' => 'contact',
    'link_display' => 'icon',
    'exclude' => 0,
    'id' => 'custom_contact',
    'table' => 'users',
    'field' => 'custom_contact',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'edit_node' => array(
    'label' => 'Edit user',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'text' => '',
    'exclude' => 0,
    'id' => 'edit_node',
    'table' => 'users',
    'field' => 'edit_node',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'ops' => array(
    'label' => 'Shown on contact page',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'link_type' => '',
    'exclude' => 0,
    'id' => 'ops',
    'table' => 'flag_content',
    'field' => 'ops',
    'relationship' => 'flag_content_rel',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'ops_1' => array(
    'label' => 'Allow anonymous contact',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'link_type' => 'toggle',
    'exclude' => 0,
    'id' => 'ops_1',
    'table' => 'flag_content',
    'field' => 'ops',
    'relationship' => 'flag_content_rel_1',
    'override' => array(
      'button' => 'Override',
    ),
  ),
));
$handler->override_option('filters', array(
  'rid' => array(
    'operator' => 'or',
    'value' => array(),
    'group' => '0',
    'exposed' => TRUE,
    'expose' => array(
      'use_operator' => 0,
      'operator' => 'rid_op',
      'identifier' => 'rid',
      'label' => 'Roles',
      'optional' => 1,
      'single' => 1,
      'remember' => 0,
      'reduce' => 0,
    ),
    'id' => 'rid',
    'table' => 'users_roles',
    'field' => 'rid',
    'relationship' => 'none',
    'reduce_duplicates' => 0,
  ),
  'flagged' => array(
    'operator' => '=',
    'value' => 'All',
    'group' => '0',
    'exposed' => TRUE,
    'expose' => array(
      'operator' => '',
      'identifier' => 'flagged',
      'label' => 'Shown on page',
      'optional' => 1,
      'remember' => 0,
    ),
    'id' => 'flagged',
    'table' => 'flag_content',
    'field' => 'flagged',
    'relationship' => 'flag_content_rel',
    'override' => array(
      'button' => 'Use default',
    ),
  ),
  'flagged_1' => array(
    'operator' => '=',
    'value' => 'All',
    'group' => '0',
    'exposed' => TRUE,
    'expose' => array(
      'operator' => '',
      'identifier' => 'flagged_1',
      'label' => 'Anonymous Contact',
      'optional' => 1,
      'remember' => 0,
    ),
    'id' => 'flagged_1',
    'table' => 'flag_content',
    'field' => 'flagged',
    'relationship' => 'flag_content_rel_1',
    'override' => array(
      'button' => 'Use default',
    ),
  ),
  'uid' => array(
    'operator' => 'in',
    'value' => '',
    'group' => '0',
    'exposed' => TRUE,
    'expose' => array(
      'use_operator' => 0,
      'operator' => 'uid_op',
      'identifier' => 'uid',
      'label' => 'User Name',
      'optional' => 1,
      'remember' => 0,
      'reduce' => 0,
    ),
    'id' => 'uid',
    'table' => 'users',
    'field' => 'uid',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
  'uid_1' => array(
    'operator' => 'not in',
    'value' => array(
      '0' => 0,
    ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'uid_1',
    'table' => 'users',
    'field' => 'uid',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'perm',
  'perm' => 'administer custom contact page',
));
$handler->override_option('cache', array(
  'type' => 'none',
));
$handler->override_option('title', 'Contact List');
$handler->override_option('items_per_page', 20);
$handler->override_option('use_pager', '1');
$handler->override_option('style_plugin', 'table');
$handler->override_option('style_options', array(
  'grouping' => '',
  'override' => 1,
  'sticky' => 0,
  'order' => 'asc',
  'columns' => array(
    'picture' => 'picture',
    'name' => 'name',
    'value' => 'name',
    'value_1' => 'name',
    'custom_contact' => 'name',
    'edit_node' => 'name',
    'value_2' => 'value_2',
    'ops_1' => 'ops_1',
    'ops' => 'ops',
    'weight' => 'weight',
  ),
  'info' => array(
    'picture' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'name' => array(
      'sortable' => 0,
      'separator' => '<br />',
    ),
    'value' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'value_1' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'custom_contact' => array(
      'separator' => '',
    ),
    'edit_node' => array(
      'separator' => '',
    ),
    'value_2' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'ops_1' => array(
      'separator' => '',
    ),
    'ops' => array(
      'separator' => '',
    ),
    'weight' => array(
      'sortable' => 0,
      'separator' => '',
    ),
  ),
  'default' => '-1',
));
$handler = $view->new_display('page', 'Administration Page', 'page_1');
$handler->override_option('sorts', array(
  'name' => array(
    'order' => 'ASC',
    'id' => 'name',
    'table' => 'users',
    'field' => 'name',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('path', 'admin/user/user/contact');
$handler->override_option('menu', array(
  'type' => 'tab',
  'title' => 'Contact Page',
  'description' => '',
  'weight' => '0',
  'name' => 'admin',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
));

$handler->override_option('header', '');
$handler->override_option('header_format', '1');
$handler->override_option('header_empty', 1);
$handler = $view->new_display('page', 'Custom Contact List', 'page_2');
$handler->override_option('sorts', array(
  'value0' => array(
    'order' => 'ASC',
    'id' => 'value0',
    'table' => 'draggableviews_structure_users0',
    'field' => 'value0',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('filters', array(
  'flagged' => array(
    'operator' => '=',
    'value' => '1',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'flagged',
    'table' => 'flag_content',
    'field' => 'flagged',
    'relationship' => 'flag_content_rel',
  ),
  'rid' => array(
    'operator' => 'not',
    'value' => array(),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'rid',
    'table' => 'users_roles',
    'field' => 'rid',
    'relationship' => 'none',
    'reduce_duplicates' => 0,
  ),
));
$handler->override_option('fields', array(
  'picture' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'exclude' => 0,
    'id' => 'picture',
    'table' => 'users',
    'field' => 'picture',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
  'name' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'link_to_user' => 1,
    'overwrite_anonymous' => 0,
    'anonymous_text' => '',
    'exclude' => 0,
    'id' => 'name',
    'table' => 'users',
    'field' => 'name',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'custom_contact' => array(
    'label' => 'Custom Contact link',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'text' => 'contact',
    'link_display' => 'icon',
    'exclude' => 0,
    'id' => 'custom_contact',
    'table' => 'users',
    'field' => 'custom_contact',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
  'value0' => array(
    'label' => 'Order',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'exclude' => 0,
    'id' => 'value0',
    'table' => 'draggableviews_structure_users0',
    'field' => 'value0',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('sorts', array(
  'value0' => array(
    'order' => 'ASC',
    'id' => 'value0',
    'table' => 'draggableviews_structure_users0',
    'field' => 'value0',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('style_plugin', 'draggabletable');
$handler->override_option('style_options', array(
  'override' => 1,
  'sticky' => 0,
  'order' => 'asc',
  'columns' => array(
    'picture' => 'picture',
    'name' => 'name',
    'custom_contact' => 'name',
    'value0' => 'value0',
  ),
  'info' => array(
    'picture' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'name' => array(
      'sortable' => 0,
      'separator' => '<br />',
    ),
    'custom_contact' => array(
      'separator' => '',
    ),
    'value0' => array(
      'sortable' => 1,
      'separator' => '',
    ),
  ),
  'default' => 'value0',
  'tabledrag_hierarchy' => array(
    'field' => 'none',
    'handler' => 'native',
  ),
  'tabledrag_order' => array(
    'field' => 'value0',
    'handler' => 'native',
  ),
  'draggableviews_extensions' => array(
    'extension_top' => '3',
    'extension_bottom' => '3',
  ),
  'tabledrag_order_visible' => array(
    'visible' => 0,
  ),
  'tabledrag_hierarchy_visible' => array(
    'visible' => 0,
  ),
  'draggableviews_depth_limit' => '-1',
  'tabledrag_types_add' => 'Add type',
  'tabledrag_expand' => array(
    'expand_links' => 'expand_links',
    'collapsed' => 0,
    'by_uid' => 0,
  ),
  'tabledrag_lock' => array(
    'lock' => 0,
  ),
  'draggableviews_default_on_top' => '1',
  'draggableviews_arguments' => array(
    'use_args' => 0,
  ),
));
$handler->override_option('path', custom_contact_path());
$handler->override_option('menu', array(
  'type' => 'normal',
  'title' => 'Contact List',
  'description' => 'A list of users who can be contacted.',
  'weight' => '0',
  'name' => 'navigation',
));
$handler->override_option('header', '');
$handler->override_option('header_format', '1');
$handler->override_option('header_empty', 1);

$views[$view->name] = $view;
return $views;
}
<?php
/**
 * A field that links to the user contact page, if access is permitted.
 */
class custom_contact_handler_field_contact_link extends views_handler_field_user_link {
  // Override 'access user profiles' access added
  // by views_handler_field_user_link, we check this
  // later in 'custom_contact_mail_link'.
  function access() {
    return TRUE;
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['link_display'] = array('default' => 'link', 'translatable' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_display'] = array(
      '#title' => t('Type of link'),
      '#default_value' => $this->options['link_display'],
      '#type' => 'select',
      '#options' => array(
        'link' => t('Link'),
        'icon' => t('Icon'),
      ),
    );
    $form['text']['#title'] = t('Link label');
    $form['text']['#required'] = TRUE;
    $form['text']['#default_value'] = empty($this->options['text']) ? t('contact') : $this->options['text'];
  }

  function render($values) {
    global $user;
    $uid = $values->{$this->aliases['uid']};
    $account = user_load(array('uid' => $uid));
    
    // Retrieve the right link for this account.
    return custom_contact_mail_link($account, $this->options['text'], $this->options['link_display']);
  }
}


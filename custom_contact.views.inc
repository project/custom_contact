<?php
/**
 * @file
 * Provide views data and handlers for contact.module
 */

function custom_contact_views_data() {
  $data['users']['custom_contact'] = array(
    'field' => array(
      'title' => t('Contact user link'),
      'help' => t('Provide a link to the user contact page which can be made visible to anonymous users.'),
      'handler' => 'custom_contact_handler_field_contact_link',
    ),
  );
  return $data;
}
/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function custom_contact_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'custom_contact'),
    ),
    'handlers' => array(
      'custom_contact_handler_field_contact_link' => array(
        'parent' => 'views_handler_field_user_link',
      ),
    ),
  );
}
